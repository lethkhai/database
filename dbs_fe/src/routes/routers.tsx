import { Navigate, useNavigate, useRoutes } from "react-router-dom";
import DashBoardLayout from "../components/Layout/DashboardLayout";
import UserAppLayout from "../components/Layout/UserAppLayout";
import SanPham from "../pages/SanPham";
import Staff from "../pages/Staff";
import Login from "../pages/Login";
// import { element } from "prop-types";

export default function Router() {
  const isUserLoggedIn = () => {
    // Thực hiện logic kiểm tra đăng nhập ở đây
    // Ví dụ: Kiểm tra xem có thông tin đăng nhập (token, user object, etc.) trong localStorage hay không
    const token = localStorage.getItem("token");
    return !!token; // Trả về true nếu có thông tin đăng nhập, ngược lại trả về false
  };
  const navigate = useNavigate();
  const handleLogin = () => {
    // Xử lý sau khi đăng nhập thành công, nếu cần
    navigate("nhan-vien");
  };
  const routes = useRoutes([
    {
      element: <UserAppLayout />,
      children: [
        {
          element: <DashBoardLayout />,
          children: [
            {
              path: "/san-pham",
              // element: isUserLoggedIn() ? <SanPham /> : <Navigate to="/" />,
              element: <SanPham/>
            },
            {
              path: "/nhan-vien",
              // element: isUserLoggedIn() ? <Staff /> : <Navigate to="/" />,
              element: <Staff/>
            },
          ],
        },
      ],
    },
    {
      path: "/",
      element: <Login onLogin={handleLogin} index:true />,
    },
  ]);

  return routes;
}
