import classNames from "classnames";
import { useEffect, useState } from "react";
import {
  AcademicCapIcon,
  BanknotesIcon,
  ExclamationTriangleIcon,
  IdentificationIcon,
  MagnifyingGlassIcon,
  MapPinIcon,
  TrashIcon,
} from "@heroicons/react/24/outline";
import axios from "axios";
import AddStaff from "./AddStaff";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function Staff() {
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [data, setData] = useState([]);
  const [search, setSearch] = useState<any>("");
  const handleSearch = (event: any) => {
    setSearch(event.target.value);
  };
  useEffect(() => {
    const fetchStaff = async () => {
      try {
        const response = await fetch(
          `http://localhost:8080/api/v2/staff?search=${search}`
        );
        const data = await response.json();
        if (data != undefined) {
          setData(data);
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchStaff();
    // console.log(search);
  }, [search]);

  const handelOnClick = (ma_nhan_vien: any) => {
    console.log("click");
    setOpen(!open);
    handleDelete(ma_nhan_vien);
    toast.success("Xoá thành công! Hãy refresh lại trang");
  };
  const handleDelete = async (ma_nhan_vien: any) => {
    const respone = await axios({
      method: "DELETE",
      url: `http://localhost:8080/api/v2/delStaff`,
      data: {
        ma_nhan_vien: ma_nhan_vien,
      },
    });
    console.log(respone);
  };

  const hoc_van = (value: any) => {
    if (value == "Cao Dang") {
      return "Cao đẳng";
    } else return "Đại học";
  };
  const task = (value: any) => {
    if (value == "Nhan Vien") {
      return "Nhân viên";
    } else return "Thu ngân";
  };

  return (
    <div className={classNames("w-full h-full")}>
      <ToastContainer />
      <div className="flex items-center sticky top-24 pb-4 px-4">
        <div className="flex bg-white rounded-lg border-2 shadow-inner">
          <div className="py-1 p-1 flex items-center justify-center border-r">
            <MagnifyingGlassIcon className="w-6 h-6 text-gray-600 " />
          </div>
          <input
            type="text"
            className="block w-full px-1 py-2 text-black bg-white focus:outline-none text-base font-extralight shadow-inner"
            placeholder="Lê Nguyễn A..."
            value={search}
            onChange={handleSearch}
          />
        </div>
      </div>

      <div className="h-full flex flex-wrap justify-evenly gap-y-4">
        {data.map((data: any, index: any) => (
          <div
            key={index}
            className={classNames(
              "w-[30%] h-fit lg:p-4 md:p-2 flex",
              "rounded-xl shadow-sm border hover:bg-gray-100 hover:shadow-xl"
            )}
          >
            <div className="w-[50%] flex justify-center items-center">
              <img
                src={"src/assets/ava3.png"}
                className="w-full rounded-l-lg"
              />
            </div>
            <div className="flex flex-col text-lg gap-y-1">
              <div className="px-2 flex flex-col">
                <div className="font-semibold py-1 ">{data.ten}</div>
                <div className="flex gap-1">
                  {data.nhanvienkho == null ? (
                    <div className="px-2 py-1 bg-green-700 rounded-lg text-white font-semibold text-sm">
                      {task(data.chuc_vu)}
                    </div>
                  ) : (
                    <>
                      <div className="px-2 py-1 bg-green-700 rounded-lg text-white font-semibold text-sm">
                        {task(data.chuc_vu)}
                      </div>
                      <div className="px-2 py-1 bg-green-700 rounded-lg text-white font-semibold text-sm">
                        {"Kho"}
                      </div>
                    </>
                  )}
                </div>
              </div>
              <div className="px-2 py-1 flex items-center">
                <IdentificationIcon className="w-6 h-6" />
                <div className="italic text-sm font-sans px-1">{data.cccd}</div>
              </div>
              <div className="px-2 py-1 flex items-center">
                <MapPinIcon className="w-6 h-6" />
                <div className="px-1 italic text-sm font-sans">
                  {data.dia_chi}
                </div>
              </div>
              <div className="px-2 py-1 flex items-center">
                <AcademicCapIcon className="w-6 h-6" />
                <div className="px-1 italic text-sm font-sans">
                  {hoc_van(data.trinh_do_hoc_van)}
                </div>
              </div>
              <div className="px-2 py-1 flex items-center">
                <BanknotesIcon className="w-6 h-6" />
                {data.nhanvienkho == null ? (
                  <div className="px-1 italic text-sm font-sans">
                    {data.luong.toLocaleString()}
                  </div>
                ) : (
                  <div className="px-1 italic text-sm font-sans flex gap-x-1">
                    {data.luong.toLocaleString() + "+"}
                    <div title="Phụ cấp">
                      {data.nhanvienkho.phucap.toLocaleString()}
                    </div>
                  </div>
                )}
              </div>
            </div>
            <button className="w-fit h-fit">
              <TrashIcon
                className="p-1 w-7 h-7 -mt-4 -mr-4 text-red-600 rounded-full hover:bg-gray-200"
                title="Delete"
                onClick={handleOpen}
              />
            </button>
            {open ? (
              <div className="modal fixed inset-0 z-10 flex flex-col items-center justify-center ">
                <div
                  className={classNames(
                    "bg-white w-fit h-fit border-2 border-gray-500 shadow-xl px-2",
                    ""
                  )}
                >
                  <div className="text-3xl font-bold flex justify-center p-4">
                    Are you sure you want to DELETE ?
                  </div>
                  <div className="flex flex-col bg-orange-300 border-orange-500 border py-4 px-2">
                    <div className="flex text-italic ">
                      <ExclamationTriangleIcon className="w-6 h-6 text-red-800" />
                      <div className="flex text-red-800 px-2 font-bold">
                        WARNING
                      </div>
                    </div>
                    <div className="italic">
                      This action cannot be undone, the deleted item cannot be
                      restored.
                    </div>
                  </div>
                  <div className="flex justify-end gap-3 py-2">
                    <button
                      className="px-4 py-2 bg-red-600 text-lg font-medium text-white rounded-lg hover:bg-red-800"
                      onClick={handleClose}
                    >
                      No
                    </button>
                    <button
                      className="px-4 py-2 bg-green-600 text-lg font-medium text-white rounded-lg hover:bg-green-800"
                      onClick={() => handelOnClick(data.ma_nhan_vien)}
                    >
                      Yes
                    </button>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        ))}
      </div>
      <div className="w-full sticky bottom-5 flex justify-end">
        <AddStaff />
      </div>
    </div>
  );
}
