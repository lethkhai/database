import classNames from "classnames";
import { useEffect, useState } from "react";
import AddFood from "./AddFood";
import {
  ExclamationTriangleIcon,
  MagnifyingGlassIcon,
  TrashIcon,
} from "@heroicons/react/24/outline";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function SanPham() {
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const [data, setData] = useState([]);
  const [search, setSearch] = useState<any>("");
  const handleSearch = (event: any) => {
    setSearch(event.target.value);
  };
  useEffect(() => {
    const fetchSanPham = async () => {
      try {
        const response = await fetch(
          `http://localhost:8080/api/v2/food?search=${search}`
        );
        const data = await response.json();
        if (data) {
          setData(data);
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchSanPham();
    console.log(data);
  }, [search]);

  const handelOnClick = (ma_mon: any) => {
    console.log("click");
    handleDelete(ma_mon);
    setOpen(!open);
    toast.success("Xoá thành công! Hãy refresh lại trang");
  };
  const handleDelete = async (ma_mon: any) => {
    const respone = await axios({
      method: "DELETE",
      url: `http://localhost:8080/api/v2/delFood`,
      data: {
        ma_mon: ma_mon,
      },
    });
    console.log(respone);
  };

  return (
    <div className={classNames("w-full h-full")}>
      <ToastContainer />
      <div
        className={classNames(
          "w-full flex flex-row items-center lg:p-4 md:p-2",
          "sticky top-[92px]",
          "border bg-gray-100 rounded-b-lg font-bold text-xl"
        )}
      >
        <div className="md:w-[10%]"></div>
        <div className="flex-grow flex mr-[5%] px-4">Tên món ăn</div>
        <div className="flex items-center justify-center">
          <div className="flex bg-white rounded-lg border shadow-inner">
            <div className="py-1 p-1 flex items-center justify-center border-r">
              <MagnifyingGlassIcon className="w-6 h-6 text-gray-600 " />
            </div>
            <input
              type="text"
              className="block w-full px-1 py-2 text-black bg-white focus:outline-none text-base font-extralight shadow-inner"
              placeholder="Mì tôm thanh long..."
              value={search}
              onChange={handleSearch}
            />
          </div>
        </div>
        <div className="lg:w-[15%] md:w-[10%] flex justify-center border-x">
          Nhóm
        </div>
        <div className="lg:w-[15%] md:w-[10%] flex justify-center">Đơn giá</div>
      </div>
      <div className="flex flex-col gap-1">
        {data.map((data: any, index: any) => (
          <div
            key={index}
            className={classNames(
              "w-full lg:p-4 md:p-2 flex",
              "rounded-xl shadow-sm border"
            )}
          >
            <div className="md:w-[10%] rounded-lg">
              <img
                src={data.hinh}
                className="h-28 w-full border border-gray-400 rounded-l-lg"
              />
            </div>
            <div className="flex-grow flex text-lg justify-stretch">
              <div className="px-4 flex flex-col flex-grow mr-[5%]">
                <div className="font-semibold py-2">{data.ten}</div>
                <div className="flex gap-1">
                  {data.nguyen_lieu.map((data: any, index: any) => (
                    <div
                      key={index}
                      className="px-2 py-1 bg-green-700 rounded-lg text-white font-semibold text-sm"
                    >
                      {data.ten}
                    </div>
                  ))}
                </div>
              </div>
              <div className="lg:w-[15%] md:w-[10%] flex justify-center">
                {data.nhom.ten_nhom}
              </div>
              {data.do_an != null ? (
                <div className="lg:w-[15%] md:w-[10%] font-sans flex justify-center">
                  {data.do_an.don_gia.toLocaleString()}
                </div>
              ) : data.kich_thuoc_nuoc_uong != null ? (
                <div className="lg:w-[15%] md:w-[10%]">
                  {data.kich_thuoc_nuoc_uong.map((data: any, index: any) => (
                    <div
                      key={index}
                      className="flex gap-2 justify-center items-center"
                    >
                      <div className="font-semibold">{data.size + ":"}</div>
                      <div className="font-sans">
                        {data.don_gia.toLocaleString()}
                      </div>
                    </div>
                  ))}
                </div>
              ) : (
                ""
              )}
            </div>
            <button className="w-fit h-fit">
              <TrashIcon
                className="p-1 w-7 h-7 -mt-4 -mr-4 text-red-600 rounded-full hover:bg-gray-200"
                title="Delete"
                onClick={handleOpen}
              />
            </button>
            {open ? (
              <div className="modal fixed inset-0 z-10 flex flex-col items-center justify-center ">
                <div
                  className={classNames(
                    "bg-white w-fit h-fit border-2 border-gray-500 shadow-xl px-2",
                    ""
                  )}
                >
                  <div className="text-3xl font-bold flex justify-center p-4">
                    Are you sure you want to DELETE ?
                  </div>
                  <div className="flex flex-col bg-orange-300 border-orange-500 border py-4 px-2">
                    <div className="flex text-italic ">
                      <ExclamationTriangleIcon className="w-6 h-6 text-red-800" />
                      <div className="flex text-red-800 px-2 font-bold">
                        WARNING
                      </div>
                    </div>
                    <div className="italic">
                      This action cannot be undone, the deleted item cannot be
                      restored.
                    </div>
                  </div>
                  <div className="flex justify-end gap-3 py-2">
                    <button
                      className="px-4 py-2 bg-red-600 text-lg font-medium text-white rounded-lg hover:bg-red-800"
                      onClick={handleClose}
                    >
                      No
                    </button>
                    <button
                      className="px-4 py-2 bg-green-600 text-lg font-medium text-white rounded-lg hover:bg-green-800"
                      onClick={() => handelOnClick(data.ma_mon)}
                    >
                      Yes
                    </button>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
        ))}
      </div>
      <div className="w-full sticky bottom-5 flex justify-end">
        <AddFood />
      </div>
    </div>
  );
}
