// Login.js
import React, { useState } from "react";
import axios from "axios";

const Login = ({ onLogin }: any) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleLogin = async (username: any, password: any) => {
    try {
      const response = await axios.post("http://localhost:8080/api/v2/login", {
        data: {
          email: username,
          password: password,
        },
      });

      const token = response.data.token;

      onLogin(token);
    } catch (error) {
      console.error("Login failed:", error);
      setError("Invalid username or password");
    }
  };

  return (
    <div className="flex h-screen flex-1 flex-col items-center lg:px-8 bg-orange-200 py-6 px-6">
      <div className="max-w-md w-full ">
      <img
            className="mx-auto h-auto w-auto"
            src="src/assets/restaurant.png"
            alt="Your Company"
          />
        <div className="text-3xl font-semibold mb-4">Login</div>
        {error && <div className="text-red-500 mb-4">{error}</div>}
        <div className="mb-4">
          <label
            htmlFor="username"
            className="block text-sm font-medium text-gray-700"
          >
            Username
          </label>
          <input
            type="text"
            id="username"
            className="mt-1 p-2 border rounded w-full"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <label
            htmlFor="password"
            className="block text-sm font-medium text-gray-700"
          >
            Password
          </label>
          <input
            type="password"
            id="password"
            className="mt-1 p-2 border rounded w-full"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button
          className="bg-blue-500 text-white p-2 rounded w-full"
          onClick={() => handleLogin(username, password)}
        >
          Login
        </button>
      </div>
    </div>
  );
};

export default Login;
