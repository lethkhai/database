import classNames from "classnames";
import { useEffect, useState } from "react";
import axios from "axios";
import React from "react";
import Select from "react-select";
import {
  Unstable_NumberInput as BaseNumberInput,
  NumberInputProps,
  NumberInputOwnerState,
} from "@mui/base/Unstable_NumberInput";
import clsx from "clsx";
import { XCircleIcon } from "@heroicons/react/24/outline";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function AddStaff() {
  const [data, setData] = useState<number>();
  const genId = (staffId: any) => {
    let Id = parseInt(staffId, 10);
    console.log(Id + 1);
    return `NV${Id + 1}`;
  };
  useEffect(() => {
    const fetchId = async () => {
      try {
        const response = await fetch(`http://localhost:8080/api/v2/staffId`);
        const data = await response.json();
        if (data != undefined) {
          setData(data[0].ma_nhan_vien.substring(2));
        }
      } catch (error) {
        console.log(error);
      }
    };
    fetchId();
    console.log(data);
  }, []);

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(!open);

  const [name, setName] = useState<any>("");
  const handleNameChange = (event: any) => {
    setName(event.target.value);
  };
  useEffect(() => {
    console.log(name);
  }, [name]);

  const [address, setAddress] = useState<any>("");
  const handleAddressChange = (event: any) => {
    setAddress(event.target.value);
  };
  useEffect(() => {
    console.log(address);
  }, [address]);

  const [cccd, setCccd] = useState<any>("");
  const handleCccdChange = (event: any) => {
    setCccd(event.target.value);
  };
  useEffect(() => {
    console.log(cccd);
  }, [cccd]);

  // input value PRICE
  const [value, setValue] = useState<any>(null);
  const handleChange = (newValue: any) => {
    setValue(newValue);
  };
  useEffect(() => {
    console.log(value);
  }, [value]);
  // end price

  const addClose = () => {
    handleButtonClick(
      genId(data),
      name,
      address,
      cccd,
      value,
      task.value,
      nhom.value
    );
    setOpen(!open);
  };
  const handleButtonClick = async (
    ma_nhan_vien: any,
    ten: any,
    dia_chi: any,
    cccd: any,
    luong: any,
    chuc_vu: any,
    trinh_do_hoc_van: any
  ) => {
    try {
      const respone = await axios({
        method: "POST",
        url: `http://localhost:8080/api/v2/addStaff`,
        data: {
          ma_nhan_vien: ma_nhan_vien,
          ten: ten,
          dia_chi: dia_chi,
          cccd: cccd,
          luong: luong,
          chuc_vu: chuc_vu,
          trinh_do_hoc_van: trinh_do_hoc_van,
        },
      });
      console.log(respone);
      if (respone.data.error) {
        toast.error(respone.data.error.message);
      } else {
        toast.success("Nhân viên đã được thêm! Hãy refresh lại trang");
      }
    } catch (error) {
      console.error(error);

      toast.error("Đã có lỗi xảy ra! Xin hãy thử lại");
    }
  };

  const group = [
    { label: "Thu ngân", value: "Thu Ngan" },
    { label: "Nhân viên", value: "Nhan Vien" },
  ];
  const [nhom, setNhom] = useState<any>(null);
  const handleGroupChange = (newValue: any) => {
    setNhom(newValue);
  };

  const school = [
    { label: "Cao đẳng", value: "Cao Dang" },
    { label: "Đại học", value: "Dai Hoc" },
  ];
  const [task, setTask] = useState<any>(null);
  const handleTaskChange = (newValue: any) => {
    setTask(newValue);
  };

  const resetData = () => {
    setNhom(null);
    setValue(null);
    setName("");
    setCccd("");
    setTask(null);
    setAddress("");
  };
  const isClose = () => {
    resetData();
    setOpen(false);
  };

  return (
    <>
      <div className="w-[100px] h-[50px]">
        <ToastContainer />
        <button
          onClick={handleOpen}
          className="relative w-[50%] h-full text-3xl font-w bg-[#05966A] hover:bg-emerald-700 text-white rounded-full transition-all duration-300 hover:w-[100%] group"
        >
          <span className="absolute left-1/2 top-1/2 transform -translate-x-1/2 text-sm -translate-y-1/2 opacity-0 transition-opacity duration-300 group-hover:opacity-100">
            Thêm
          </span>
          <span className="absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 opacity-100 transition-opacity duration-300 group-hover:opacity-0">
            +
          </span>
        </button>
      </div>
      {open ? (
        <div className="modal fixed inset-0 z-10 flex flex-col items-center justify-center backdrop-blur-sm">
          <div
            className={classNames(
              "bg-white w-[50%] h-fit border-2 shadow-lg",
              "rounded-2xl"
            )}
          >
            <div className="w-full flex justify-end -mt-2">
              <button onClick={handleOpen} className="z-20">
                <XCircleIcon className="w-6 h-6 z-20 text-white rounded-full bg-black" />
              </button>
            </div>

            <div className="px-4">
              <div className="font-bold text-4xl w-full flex justify-center lg:p-4 md:p-2 border-b">
                THÊM NHÂN VIÊN
              </div>

              <div className="flex flex-col lg:gap-y-6 md:gap-y-4">
                <div className="flex w-full justify-between lg:pt-6 md:pt-4">
                  <div className="w-[60%]">
                    <div className="font-semibold text-lg">Họ & Tên</div>
                    <input
                      className="shadow appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      id="foodname"
                      type="text"
                      placeholder="Lê Võ Đặng"
                      value={name}
                      onChange={handleNameChange}
                    />
                  </div>

                  <div className="flex flex-col w-[30%]">
                    <div className="font-semibold text-lg">Chức vụ</div>
                    <Select
                      options={group}
                      isMulti={false}
                      value={task}
                      placeholder={"Chức vụ"}
                      onChange={handleTaskChange}
                    />
                  </div>
                </div>

                <div className="flex justify-between">
                  <div className="w-[60%]">
                    <div className="font-semibold text-lg">Địa chỉ</div>
                    <input
                      className="shadow appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      id="address"
                      type="text"
                      placeholder="123 Duong ABC, Quan 1"
                      value={address}
                      onChange={handleAddressChange}
                    />
                  </div>

                  <div className="flex flex-col w-[30%]">
                    <div className="font-semibold text-lg">Học vấn</div>
                    <Select
                      options={school}
                      isMulti={false}
                      value={nhom}
                      placeholder={"Học vấn"}
                      onChange={handleGroupChange}
                    />
                  </div>
                </div>

                <div className="flex justify-between">
                  <div className="w-[60%]">
                    <div className="font-semibold text-lg">
                      Căn cước công dân
                    </div>
                    <input
                      className="shadow appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline resize-none"
                      id="cccd"
                      type="number"
                      placeholder="123456789012"
                      value={cccd}
                      onChange={handleCccdChange}
                    />
                  </div>
                  <div className="w-[30%]">
                    <div className="font-semibold text-lg">Mức lương</div>
                    <NumberInput
                      placeholder="5.000.000"
                      min={1000000}
                      max={30000000}
                      value={value ? value.toLocaleString() : value}
                      onChange={(event, newValue) => handleChange(newValue)}
                    />
                  </div>
                </div>
              </div>
              <div className="flex w-full justify-center lg:p-6 md:p-4 gap-x-2">
                <button
                  className="border px-4 py-2 rounded-md bg-red-400 hover:bg-red-700"
                  onClick={isClose}
                >
                  Huỷ
                </button>
                <button
                  className="border px-4 py-2 rounded-md bg-green-500 hover:bg-green-700"
                  onClick={addClose}
                >
                  Thêm
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}

const resolveSlotProps = (fn: any, args: any) =>
  typeof fn === "function" ? fn(args) : fn;

const NumberInput = React.forwardRef(function NumberInput(
  props: NumberInputProps,
  ref: React.ForwardedRef<HTMLDivElement>
) {
  return (
    <BaseNumberInput
      {...props}
      ref={ref}
      slotProps={{
        root: (ownerState: NumberInputOwnerState) => {
          const resolvedSlotProps = resolveSlotProps(
            props.slotProps?.root,
            ownerState
          );
          return {
            ...resolvedSlotProps,
            className: clsx(
              "border shadow flex overflow-hidden font-sans rounded-md text-slate-900 bg-white focus-visible:outline-0 w-full h-fit",
              resolvedSlotProps?.className
            ),
          };
        },
        input: (ownerState: NumberInputOwnerState) => {
          const resolvedSlotProps = resolveSlotProps(
            props.slotProps?.input,
            ownerState
          );
          return {
            ...resolvedSlotProps,
            className: classNames(
              "h-full",
              "leading-normal text-slate-900 bg-inherit border-0 rounded-md px-2 py-2 outline-0 focus-visible:outline-0 focus-visible:outline-none",
              resolvedSlotProps?.className
            ),
          };
        },
      }}
    />
  );
});
