import classNames from "classnames";
// import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { XCircleIcon } from "@heroicons/react/24/solid";

export default function Plant() {
  // const navigate = useNavigate();
  // const ToRose = () => {
  //   navigate("./rose");
  // };
  const [isOpen, setIsOpen] = useState(false);
  console.log(isOpen);

  const plant = [
    {
      name: "Rose",
      scientific_name: "Rosa",
      type: "Perennial",
      sunlight_requirement: "Full Sun",
      watering_schedule: "Regular",
      description:
        "Roses are classic garden flowers known for their beauty and fragrance. They come in various colors and are often used in landscaping and as cut flowers.",
      img_url: "/src/assets/rose_pic.jpg",
      requirements: {
        temperature: {
          min: 18,
          max: 25,
          ideal: 22,
        },
        humidity: {
          min: 40,
          max: 60,
          ideal: 50,
        },
        soil: {
          type: "Well-drained",
          pH: 6.0 - 6.5,
          moisture: "Medium",
        },
      },
    },
    {
      name: "Tomato",
      scientific_name: "Solanum lycopersicum",
      type: "Annual",
      sunlight_requirement: "Full Sun",
      watering_schedule: "Regular",
      description:
        "Tomatoes are popular vegetables that thrive in sunny locations. They are versatile in the kitchen, used in salads, sauces, and a variety of dishes.",
      img_url: "/src/assets/tomato_pic.jpg",
      requirements: {
        temperature: {
          min: 20,
          max: 30,
          ideal: 25,
        },
        humidity: {
          min: 40,
          max: 80,
          ideal: 60,
        },
        soil: {
          type: "Well-drained",
          pH: 6.0 - 6.8,
          moisture: "Medium",
        },
      },
    },
    {
      name: "Lavender",
      scientific_name: "Lavandula",
      type: "Perennial",
      sunlight_requirement: "Full Sun",
      watering_schedule: "Moderate",
      description:
        "Lavender is a fragrant herb with beautiful purple flowers. It is known for its soothing aroma and is often used in aromatherapy, as well as for culinary purposes.",
      img_url: "",
      requirements: {
        temperature: {
          min: 15,
          max: 30,
          ideal: 25,
        },
        humidity: {
          min: 30,
          max: 70,
          ideal: 50,
        },
        soil: {
          type: "Well-drained",
          pH: 6.0 - 8.0,
          moisture: "Low to Medium",
        },
      },
    },
    {
      name: "Sunflower",
      scientific_name: "Helianthus annuus",
      type: "Annual",
      sunlight_requirement: "Full Sun",
      watering_schedule: "Regular",
      description:
        "Sunflowers are tall, cheerful flowers that are easy to grow. They are known for their large, vibrant yellow blooms and are a favorite in gardens and as cut flowers.",
      img_url: "",
      requirements: {
        temperature: {
          min: 18,
          max: 30,
          ideal: 25,
        },
        humidity: {
          min: 40,
          max: 70,
          ideal: 60,
        },
        soil: {
          type: "Well-drained",
          pH: 6.0 - 7.5,
          moisture: "Medium",
        },
      },
    },
  ];

  return (
    <div className={classNames("w-full h-full flex-col")}>
      <div className={classNames("flex flex-wrap justify-evenly gap-y-5")}>
        {plant.map((plant: any, index) => (
          <div
            key={index}
            className={classNames(
              "border-2 w-[30%] h-2/5 rounded-2xl shadow-md hover:shadow-2xl hover:bg-gray-100",
              "flex-col"
            )}
          >
            <div onClick={() => setIsOpen(true)}>
              <img
                src={plant.img_url}
                className={classNames(
                  "rounded-t-2xl w-full aspect-video h-full"
                  // "transition duration-300 hover:scale-125"
                )}
              />
              <div className={classNames("py-4 px-5")}>
                <div className={classNames("flex items-center")}>
                  <div className={classNames("font-bold text-2xl")}>
                    {plant.name}
                  </div>
                  <div className={classNames("px-2")}>
                    <div
                      className={classNames(
                        "font-semibold text-white text-xs justify-center items-center",
                        "rounded-xl bg-green-700 w-fit py-1 px-2 h-fit"
                      )}
                    >
                      {"Flower"}
                    </div>
                  </div>
                </div>
                <div className={classNames("")}>{plant.description}</div>
                <div></div>
              </div>
            </div>
            {/* ##### */}
            {/* Popup */}
            {isOpen == true ? (
              <div className="modal fixed inset-0 z-10 flex flex-col items-center justify-center backdrop-blur-md">
                <div className={classNames("h-5/6 w-3/5 rounded-2xl")}>
                  <div className="w-full flex items-center justify-end -mt-6">
                    <button onClick={() => setIsOpen(false)} className="z-20">
                      <XCircleIcon className="w-6 h-6 z-20 text-white rounded-full bg-black" />
                    </button>
                  </div>
                  <div
                    className={classNames(
                      "rounded-2xl w-full h-full -mt-6",
                      "border bg-white"
                    )}
                  >
                    <div className={classNames("h-2/5 w-full")}>
                      <img
                        src="/src/assets/rose_pic.jpg"
                        className={classNames("rounded-t-2xl w-full h-full")}
                      />
                    </div>
                    <div className="p-5">
                      <div>{plant.type}</div>
                      <div>{plant.description}</div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            {/* End Popup */}
          </div>
        ))}
      </div>
    </div>
  );
}
