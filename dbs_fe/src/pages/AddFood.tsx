import classNames from "classnames";
import { useEffect, useState } from "react";
import axios from "axios";
import React from "react";
import Select from "react-select";
import {
  Unstable_NumberInput as BaseNumberInput,
  NumberInputProps,
  NumberInputOwnerState,
} from "@mui/base/Unstable_NumberInput";
import clsx from "clsx";
import { XCircleIcon } from "@heroicons/react/24/outline";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function AddFood() {
  const [nhom, setNhom] = useState<any>(null);
  const handleGroupChange = (newValue: any) => {
    setNhom(newValue);
    setMamon(generateMaMon(newValue.value));
  };

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(!open);

  const [url, setUrl] = useState("");
  const handleURLChange = (e: any) => {
    const inputUrl = e.target.value;
    setUrl(inputUrl);
  };

  const [name, setName] = useState<any>("");
  const handleNameChange = (event: any) => {
    setName(event.target.value);
  };

  // input value PRICE
  const [value, setValue] = useState<any>(null);
  const handleChange = (newValue: any) => {
    setValue(newValue);
  };
  // end price

  const addClose = () => {
    handleButtonClick(maMon, name, url, nhom.value, nguyenLieu, value);
    setOpen(!open);
  };
  const handleButtonClick = async (
    ma_mon: any,
    ten: any,
    hinh: any,
    ma_nhom: any,
    ma_nguyen_lieu: any,
    don_gia: any
  ) => {
    try {
      const respone = await axios({
        method: "POST",
        url: `http://localhost:8080/api/v2/addFood`,
        data: {
          ma_mon: ma_mon,
          ten: ten,
          hinh: hinh,
          ma_nhom: ma_nhom,
          ma_nguyen_lieu: ma_nguyen_lieu,
          don_gia: don_gia,
        },
      });
      if (respone.data.error) {
        toast.error(respone.data.error.message);
      } else {
        toast.success("Thêm thành công! Hãy refresh lại trang");
      }
    } catch (error) {
      console.error(error);

      toast.error("Đã có lỗi xảy ra! Xin hãy thử lại");
    }
  };

  const group = [
    { label: "Bánh", value: "1" },
    { label: "Cơm", value: "2" },
    { label: "Coffee", value: "3" },
    { label: "Trà", value: "4" },
    { label: "Nước ép & sinh tố", value: "5" },
  ];

  const ingredient = [
    { value: "NL0001", label: "Đường" },
    { value: "NL0002", label: "Muối" },
    { value: "NL0003", label: "Bột mì" },
    { value: "NL0004", label: "Tỏi" },
    { value: "NL0005", label: "Sữa tươi" },
    { value: "NL0006", label: "Trứng gà" },
    { value: "NL0007", label: "Nước mắm" },
  ];

  const [ingred, setIngred] = useState<any>([]);
  const [nguyenLieu, setNguyenLieu] = useState<any>([
    ingred.map((data: any) => data.value),
  ]);
  const handleIngredChange = (newValue: any) => {
    setIngred(newValue);
    setNguyenLieu(newValue.map((data: any) => data.value));
  };

  // useEffect(() => {
  //   console.log(ingred);
  //   console.log(nguyenLieu);
  // }, [ingred]);

  const resetData = () => {
    setNhom(null);
    setIngred([]);
    setValue(null);
    setName("");
    setUrl("");
  };
  const isClose = () => {
    resetData();
    setOpen(false);
  };

  const [maMon, setMamon] = useState<any>(null);
  const [monCounter, setMonCounter] = useState(generateRandomValue);
  const generateMaMon = (nhom: any) => {
    setMonCounter(monCounter + 1);
    return `F${nhom}${monCounter}`;
  };
  useEffect(() => {
    // Nếu bạn muốn sinh giá trị ngẫu nhiên mới sau mỗi lần render, bạn có thể đặt lại giá trị trong hàm useEffect
    setMonCounter(generateRandomValue);
  }, []);
  function generateRandomValue() {
    // Sinh số ngẫu nhiên từ 100 đến 999
    return Math.floor(Math.random() * (999 - 100 + 1)) + 100;
  }

  useEffect(() => {
    console.log(maMon);
    console.log(nguyenLieu);
  }, []);

  return (
    <>
      <ToastContainer />
      <div className="w-[100px] h-[50px]">
        <button
          onClick={handleOpen}
          className="relative w-[50%] h-full text-3xl font-w bg-[#05966A] hover:bg-emerald-700 text-white rounded-full transition-all duration-300 hover:w-[100%] group"
        >
          <span className="absolute left-1/2 top-1/2 transform -translate-x-1/2 text-sm -translate-y-1/2 opacity-0 transition-opacity duration-300 group-hover:opacity-100">
            Thêm
          </span>
          <span className="absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 opacity-100 transition-opacity duration-300 group-hover:opacity-0">
            +
          </span>
        </button>
      </div>
      {open ? (
        <div className="modal fixed inset-0 z-10 flex flex-col items-center justify-center backdrop-blur-sm">
          <div
            className={classNames(
              "bg-white w-[50%] h-fit border-2 shadow-lg",
              "rounded-2xl"
            )}
          >
            <div className="w-full flex justify-end -mt-2">
              <button onClick={handleOpen} className="z-20">
                <XCircleIcon className="w-6 h-6 z-20 text-white rounded-full bg-black" />
              </button>
            </div>

            <div className="px-4">
              <div className="font-bold text-4xl w-full flex justify-center lg:p-4 md:p-2 border-b">
                THÊM MÓN
              </div>

              <div className="flex flex-col lg:gap-y-6 md:gap-y-4">
                <div className="flex w-full justify-between lg:pt-6 md:pt-4">
                  <div className="w-[60%]">
                    <div className="font-semibold text-lg">Tên món</div>
                    <input
                      className="shadow appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      id="foodname"
                      type="text"
                      placeholder="Mì tôm thanh long"
                      value={name}
                      onChange={handleNameChange}
                    />
                  </div>
                  <div className="w-[30%]">
                    <div className="font-semibold text-lg">Giá</div>
                    <NumberInput
                      placeholder="Giá..."
                      min={5000}
                      max={10000000}
                      value={value ? value.toLocaleString() : value}
                      onChange={(event, newValue) => handleChange(newValue)}
                    />
                  </div>
                </div>

                <div className="flex justify-between">
                  <div className="w-[60%]">
                    <div className="font-semibold text-lg">Nguyên liệu</div>
                    <Select
                      options={ingredient}
                      isMulti={true}
                      value={ingred}
                      placeholder={"Nguyên liệu"}
                      onChange={handleIngredChange}
                    />
                  </div>

                  <div className="flex flex-col w-[30%]">
                    <div className="font-semibold text-lg">Nhóm</div>
                    <Select
                      options={group}
                      isMulti={false}
                      value={nhom}
                      placeholder={"Nhóm"}
                      onChange={handleGroupChange}
                    />
                  </div>
                </div>

                <div className="">
                  <div className="font-semibold text-lg">Hình</div>
                  <input
                    className="shadow appearance-none border rounded-md w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="pic"
                    type="url"
                    value={url}
                    onChange={handleURLChange}
                    placeholder="https://example.picture.test/"
                  />
                </div>
              </div>
              <div className="flex w-full justify-center lg:p-6 md:p-4 gap-x-2">
                <button
                  className="border px-4 py-2 rounded-md bg-red-400 hover:bg-red-600"
                  onClick={isClose}
                >
                  Huỷ
                </button>
                <button
                  className="border px-4 py-2 rounded-md bg-green-500 hover:bg-green-700"
                  onClick={addClose}
                  // onClick={() => handleButtonClick(maMon, name, url, nhom.value, nguyenLieu, value)}
                >
                  Thêm
                </button>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}

const resolveSlotProps = (fn: any, args: any) =>
  typeof fn === "function" ? fn(args) : fn;

const NumberInput = React.forwardRef(function NumberInput(
  props: NumberInputProps,
  ref: React.ForwardedRef<HTMLDivElement>
) {
  return (
    <BaseNumberInput
      {...props}
      ref={ref}
      slotProps={{
        root: (ownerState: NumberInputOwnerState) => {
          const resolvedSlotProps = resolveSlotProps(
            props.slotProps?.root,
            ownerState
          );
          return {
            ...resolvedSlotProps,
            className: clsx(
              "border shadow flex overflow-hidden font-sans rounded-md text-slate-900 bg-white focus-visible:outline-0 w-full h-fit ",
              resolvedSlotProps?.className
            ),
          };
        },
        input: (ownerState: NumberInputOwnerState) => {
          const resolvedSlotProps = resolveSlotProps(
            props.slotProps?.input,
            ownerState
          );
          return {
            ...resolvedSlotProps,
            className: classNames(
              "h-full",
              "leading-normal text-slate-900 bg-inherit border-0 rounded-md px-2 py-2 outline-0 focus-visible:outline-0 focus-visible:outline-none",
              resolvedSlotProps?.className
            ),
          };
        },
      }}
    />
  );
});
