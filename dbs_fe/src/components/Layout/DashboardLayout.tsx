import { Outlet } from "react-router-dom";
import Sidebar from "../Sidebar/Sidebar";
import RightSidebar from "../Sidebar/RightSidebar";

export default function DashBoardLayout() {
  return (
    <div className="flex">
      <Sidebar />
      <div className="flex-1 p-5">
        <Outlet />
      </div>
      {/* <RightSidebar /> */}
    </div>
  );
}
