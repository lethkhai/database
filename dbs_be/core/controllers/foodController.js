const { json } = require('sequelize');
const { getAllFood, addFood, addCheBien, getIngredian, addPrice, delPrice, delCheBien, delOneFood, getAllStaff, getIdLastStaff, addNhanVien, delNhanVien, handleUserLogin } = require('../services/foodServices');


const displayAllFood = async (req, res) => {
    let search = req.query.search;
    const listFood = await getAllFood(search);
    return res.status(200).json(listFood);
};

const displayAllIngredian = async (req, res) => {
    const listIngred = await getIngredian();
    return res.status(200).json(listIngred);
}

const addingFood = async (req, res) => {
    let ma_mon = req.body.ma_mon;
    let ten = req.body.ten;
    let hinh = req.body.hinh;
    let ma_nhom = req.body.ma_nhom;
    let ma_nguyen_lieu = req.body.ma_nguyen_lieu;
    let don_gia = req.body.don_gia;
    const cheBienData = ma_nguyen_lieu.map((ma_nguyen_lieu) => ({
        ma_mon_tmpcc: ma_mon,
        ma_nguyen_lieu_tmpcc: ma_nguyen_lieu,
      }));
    const add = await addFood(ma_mon, ten, hinh, ma_nhom);
    await addCheBien(cheBienData);
    await addPrice(don_gia, ma_mon);
    if (add) {
        return res.status(404).send("Error")
    } 
    return res.send("Update info succeed!").status(200)
};

const delFood = async (req, res) => {
    let ma_mon = req.body.ma_mon;
    await delCheBien(ma_mon);
    await delPrice(ma_mon);
    const del = await delOneFood(ma_mon);
    if (del) {
        return res.send("Error").status(404)
    } 
    return res.send("Delete info succeed!").status(200)
}

const listStaff = async (req, res) => {
    let search = req.query.search;
    const listStaff = await getAllStaff(search);
    return res.status(200).json(listStaff);
};

const staffId = async (req, res) => {
    const Id = await getIdLastStaff();
    return res.status(200).json(Id);
};


const addStaff = async (req, res) => {
    let ma_nhan_vien = req.body.ma_nhan_vien;
    let ten = req.body.ten;
    let dia_chi = req.body.dia_chi;
    let cccd = req.body.cccd;
    let luong = req.body.luong;
    let chuc_vu = req.body.chuc_vu;
    let trinh_do_hoc_van = req.body.trinh_do_hoc_van;
    const add = await addNhanVien(ma_nhan_vien, ten, dia_chi, cccd, luong, chuc_vu, trinh_do_hoc_van);
    if (add) {
        return res.status(404).send("Error")
    } 
    return res.send("Update info succeed!").status(200)
};

const deleteStaff = async (req, res) => {
    let ma_nhan_vien = req.body.ma_nhan_vien;
    const del = await delNhanVien(ma_nhan_vien);
    if (del) {
        return res.send("Error").status(404)
    } 
    return res.send("Delete info succeed!").status(200)
};

const handleLogin = async (req, res) => {
    let email = req.body.email;
    let password = req.body.password;
    // if (!email || !password) {
    //     return res.status(500).json({
    //         errCode: 1,
    //         message: "Missing email or password!"
    //     })
    // }
    const user = await handleUserLogin(email, password);

    return res.status(200).json({
        result: user
    })
}

module.exports = {
    displayAllFood,
    addingFood,
    delFood,
    displayAllIngredian,
    listStaff,
    staffId,
    addStaff,
    deleteStaff,
    handleLogin,
}