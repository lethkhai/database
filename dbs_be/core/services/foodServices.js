const e = require('express');
const supabase = require('../config/supabaseClient');

const getAllFood = async (search) => {
    const { data, error } = await supabase
    .from('mon_an')
    .select(`
    *,
    do_an (don_gia),
    nhom (ten_nhom),
    nguyen_lieu (ten),
    kich_thuoc_nuoc_uong(size, don_gia)
    `)
    .ilike('ten', '%' + search + '%');
    if (error) {
        return error;
    }
    return data;
};

const getIngredian = async () => {
    const {data, error} = await supabase
    .from('nguyen_lieu')
    .select(`*`)
    if (error) {
        return error;
    }
    return data;
}

const addFood = async (ma_mon, ten, hinh, ma_nhom) => {
    const { error } = await supabase
    .from('mon_an')
    .insert({ ma_mon: ma_mon, ten: ten, hinh: hinh, ma_nhom: ma_nhom })
    if (error) {
        console.log(error);
        return error;
    }
};

const addPrice = async (don_gia, ma_do_an) => {
    const { error } = await supabase
    .from('do_an')
    .insert({ don_gia: don_gia, ma_do_an: ma_do_an})
    if (error) {
        console.log(error);
        return error;
    }
};

const addCheBien = async (cheBienData) => {
    const {error} = await supabase
    .from('che_bien')
    .insert(cheBienData)
        if (error) {
        console.log(error);
        return error;
    }
};

const delOneFood = async (ma_mon) => {
    const { error } = await supabase
    .from('mon_an')
    .delete()
    .eq('ma_mon', ma_mon)

    if (error) {
        console.log(error);
        return error;
    }
};

const delPrice =async (ma_do_an) => {
    const {error} = await supabase
    .from('do_an')
    .delete()
    .eq('ma_do_an', ma_do_an)
        if (error) {
        console.log(error);
        return error;
    }
};

const delCheBien =async (ma_mon_tmpcc) => {
    const {error} = await supabase
    .from('che_bien')
    .delete()
    .eq('ma_mon_tmpcc', ma_mon_tmpcc)
        if (error) {
        console.log(error);
        return error;
    }
};

const getAllStaff = async (search) => {
    const {data, error} = await supabase
    .from('nhanvien')
    .select(`
    *,
    nhanvienkho(phucap, giobatdau, gioketthuc),
    thungan(ca_lam_viec, ngay, gio)`)
    .ilike('ten', '%' + search + '%');
    if (error) {
        console.log(error);
        return error;
    }
    return data;
};

const getIdLastStaff = async () => {
    const { data, error } = await supabase
    .from('nhanvien')
    .select(`ma_nhan_vien`)
    .order(`ma_nhan_vien`, { ascending: false })
    .limit(1)
    if (error) {
        console.log(error);
        return error;
    }
    return data;
};

const addNhanVien = async (ma_nhan_vien, ten, dia_chi, cccd, luong, chuc_vu, trinh_do_hoc_van) => {
    const {error} = await supabase
    .from('nhanvien')
    .insert({ma_nhan_vien: ma_nhan_vien, ten: ten, dia_chi: dia_chi, cccd: cccd, luong: luong, chuc_vu: chuc_vu, trinh_do_hoc_van: trinh_do_hoc_van})
        if (error) {
        console.log(error);
        return error;
    }
};

const delNhanVien = async (ma_nhan_vien) => {
    const {error} = await supabase
    .from('nhanvien')
    .delete()
    .eq(`ma_nhan_vien`, ma_nhan_vien)
    if (error) {
        console.log(error);
        return error;
    }
};

async function signUpNewUser() {
    const { data, error } = await supabase.auth.signUp({
      email: 'example@email.com',
      password: 'example-password',
      options: {
        emailRedirectTo: 'https//example.com/welcome'
      }
    })
  }

  const handleUserLogin = async (email, password) => {
    const { data: user, error } = await supabase
        .from('account') // Thay 'your_user_table_name' bằng tên bảng người dùng thực tế
        .select('password') // Lấy trường password và các trường khác bạn cần
        .eq('email', email)
        .single();
    if (error) {
        console.error(error.message);
        return {
            errCode: 1,
            errMessage: 'An error occurred while fetching user data.'
        };
    }
    if (!user) {
        return {
            errCode: 1,
            errMessage: 'Email not exist!'
        };
    }
    if (user.password !== password) {
        return {
            errCode: 1,
            errMessage: 'Incorrect password!'
        };
    }
    return {
        Message: 'Login successfully!'
    };
};

async function signOut() {
    const { error } = await supabase.auth.signOut()
  };



module.exports = {
    getAllFood,
    getIngredian,
    addFood,
    addPrice,
    addCheBien,
    delOneFood,
    delPrice,
    delCheBien,
    getAllStaff,
    getIdLastStaff,
    addNhanVien,
    delNhanVien,
    handleUserLogin,
    signUpNewUser,
    signOut,
}