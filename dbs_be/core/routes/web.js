const express = require("express");
const router = express.Router();
const { displayAllFood, addingFood, displayAllIngredian, delFood, listStaff, staffId, addStaff, deleteStaff, handleLogin } = require("../controllers/foodController");
const { signOut } = require("../services/foodServices");

const initWebRoutes = (app) => {
  router.get("/api/v2/food", displayAllFood);
  router.get("/api/v2/ingred", displayAllIngredian)
  router.delete("/api/v2/delFood", delFood)
  router.post("/api/v2/addFood", addingFood);
  router.get("/api/v2/staff", listStaff);
  router.get("/api/v2/staffId", staffId);
  router.post("/api/v2/addStaff", addStaff);
  router.delete("/api/v2/delStaff", deleteStaff);
  // USER
  router.post('/api/v2/login', handleLogin);
  router.post('/api/v2/logout', signOut);
  return app.use("/", router);
};

module.exports = initWebRoutes;
